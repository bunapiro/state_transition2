#include <stdio.h>
#include "../../include/local.h"

#include "state_machine.h"

/*******************************************************************/
/************************** 各種変数定義 ****************************/
/*******************************************************************/

//表示用のイベント名
const char *EventName[] = {
    "ラーメン食え",
    "タピオカ飲め",
    "胃薬飲め",
    "食事完了",
};

//表示用のステータス名
const char *StateName[] = {
    "空腹",
    "食事中",
    "満腹",
};

//状態遷移テーブル ★肝になる所
const STATE_ROW_T StateCmdTable[ST_NUM] = {
                /* ラーメン     タピオカ 　　　　 胃薬　　　　 食事完了   */
/* 空腹　 */    [ST_KUFUKU]     = {GoToSyokujichu,GoToManpuku,    Refuse,     DoNothing  } ,
/* 食事中 */    [ST_SYOKUJICHU] = {Refuse,        Refuse,         Refuse,     GoToManpuku} ,
/* 満腹 　*/    [ST_MANPUKU]    = {Refuse,        GoToSyokujichu, GoToKufuku, DoNothing  }
};

PRIVATE MY_STATE_T m_CurrentState = ST_KUFUKU; //現在ステータス(空腹で初期化)


//食事中への遷移処理
PUBLIC void GoToSyokujichu( EVENT_T event )
{
    PrintEvent(event);
    puts("食事中へ遷移");
    SetState(ST_SYOKUJICHU);
    PrintState();
}

//満腹への遷移処理
PUBLIC void GoToManpuku( EVENT_T event )
{
    PrintEvent(event);
    puts("満腹へ遷移");
    SetState(ST_MANPUKU);
    PrintState();
}

//空腹への遷移処理
PUBLIC void GoToKufuku( EVENT_T event )
{
    PrintEvent(event);
    puts("空腹へ遷移");
    SetState(ST_KUFUKU);
    PrintState();
}

//拒否の処理
PUBLIC void Refuse( EVENT_T event )
{
    PrintEvent(event);
    puts("嫌だ！断固拒否！");
    PrintState();
}

//異常時に何もしない為の処理
PUBLIC void DoNothing( EVENT_T event )
{
    PrintEvent(event);
    puts("何かがおかしい。何もしない");
    PrintState();
}

//現在状態の表示
PUBLIC void PrintState( void )
{
    printf("現在のステータス: %s\n\n", StateName[ m_CurrentState ]);
}

//イベント名の表示
PUBLIC void PrintEvent( EVENT_T event )
{
    printf("イベント: %s\n", EventName[event]);
}

//現在状態の設定
PUBLIC void SetState( MY_STATE_T st )
{
    m_CurrentState = st;
}

//メイン処理
int main(int argc, char *argv[])
{
    EVENT_T event;

    puts("スタート");
    PrintState();  //初期ステータスの表示

    //胃薬飲め
    event = EVENT_IGUSURI;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //ラーメンを食え
    event = EVENT_RAMEN;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //食事完了
    event = EVENT_SYOKUJI_COMP;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //ラーメンを食え
    event = EVENT_RAMEN;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //タピオカを食え
    event = EVENT_TAPIOCA;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //食事完了
    event = EVENT_SYOKUJI_COMP;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //胃薬を飲め
    event = EVENT_IGUSURI;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    //食事完了
    event = EVENT_SYOKUJI_COMP;
    StateCmdTable[ m_CurrentState ].senni_func[event](event);

    return 0;
}
