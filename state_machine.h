#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

// 状態一覧
typedef enum {
    ST_KUFUKU,          //空腹
    ST_SYOKUJICHU,      //食事中
    ST_MANPUKU,         //満腹
    ST_NUM,             //状態の数
} MY_STATE_T ;

// イベント一覧
typedef enum {
    EVENT_RAMEN,          //ラーメン食え
    EVENT_TAPIOCA,        //タピオカ飲め
    EVENT_IGUSURI,        //胃薬飲め
    EVENT_SYOKUJI_COMP,   //食事完了
    EVENT_NUM,            //コマンドの数
} EVENT_T;

//イベントの関数型宣言
typedef void ( *SENNI_FUNC )( EVENT_T event);

//状態遷移表の1列分
typedef struct {
    SENNI_FUNC senni_func[EVENT_NUM];
} STATE_ROW_T;

//プロトタイプ宣言
PUBLIC void GoToSyokujichu( EVENT_T event );
PUBLIC void GoToManpuku( EVENT_T event );
PUBLIC void GoToKufuku( EVENT_T event );
PUBLIC void Refuse( EVENT_T event );
PUBLIC void DoNothing( EVENT_T event );
PUBLIC void PrintEvent( EVENT_T event);
PUBLIC void PrintState( void );
PUBLIC void SetState(MY_STATE_T st);

#endif  /* __STATE_MACHINE_H__ */
